package org.digitalrats

import org.digitalrats.flux.FluxClient
import sttp.client3._
import sttp.model._
import zio.{Task, ZIO}
import zio.test.Assertion._
import zio.test._

object FluxTest extends DefaultRunnableSpec {
  final case class Address(country: String, city: String)
  final case class User(name: String, age: Int, address: Address)
  def spec: Spec[Any, TestFailure[Throwable], TestSuccess] = suite("Flux Exercise")(
    testM("Sttp backend checking") {
      assertM(
         ZIO.accessM[FluxClient.Service](
          a => {
            for {
              rsp <- basicRequest.get(uri"https://www.google.ru").send(a.get)
              _ <- Task.effect{
                println(rsp)
              }
              status: StatusCode = rsp.code
            } yield status.isSuccess
          }
        ).provideLayer(FluxClient.live)
      )(isTrue)
    }
  )
}
