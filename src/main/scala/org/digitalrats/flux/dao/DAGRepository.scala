package org.digitalrats.flux.dao

import org.digitalrats.flux.configuration.ConfigurationModule.Configuration
import org.digitalrats.flux.model.DAG
import zio.blocking.Blocking
import zio.{Has, Task, URLayer, ZIO}

object DAGRepository {
  case class ErrorInfo(message: String)

  trait Service extends Repository[ErrorInfo, DAG]

  type DAGRepository = Has[Service]

  final case class DAGRepositoryImpl(blocking: Blocking.Service) extends Service {

    private val  db: Map[String,DAG] = Map("0"->DAG())

    override def find(id: String): Task[Either[ErrorInfo, DAG]] =
      Task(db.get(id).toRight(ErrorInfo("Unknown")))

    override def findAll(): Task[Either[ErrorInfo, Seq[DAG]]] =
      Task(Right(db.values.toSeq))
  }

  val live: URLayer[Blocking with Configuration, DAGRepository] =
    (DAGRepositoryImpl(_)).toLayer[Service]

  def find(id: String): ZIO[DAGRepository, Throwable, Either[ErrorInfo,DAG]] =
    ZIO.accessM(_.get.find(id))

  def findAll(): ZIO[DAGRepository, Throwable, Either[ErrorInfo,Seq[DAG]]] =
    ZIO.accessM(_.get.findAll())
}
