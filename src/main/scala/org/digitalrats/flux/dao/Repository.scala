package org.digitalrats.flux.dao

import zio.Task

trait Repository[E, R] {
  def find(id: String): Task[Either[E, R]]
  def findAll(): Task[Either[E,Seq[R]]]
}
