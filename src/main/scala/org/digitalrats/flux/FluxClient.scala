package org.digitalrats.flux

import org.http4s.ember.client.EmberClientBuilder
import sttp.capabilities.fs2.Fs2Streams
import sttp.client3._
import sttp.client3.http4s._
import zio.interop.catz._
import zio.interop.catz.implicits._
import zio.{Has, Task, ZLayer}

object FluxClient {
  type Service = Has[SttpBackend[Task, Fs2Streams[Task]]]
  val live: ZLayer[Any, Throwable, Service] = ZLayer.fromManaged {
    (for {
      client <- EmberClientBuilder.default[Task].build
      sttpClient = Http4sBackend.usingClient(client)
    } yield sttpClient).toManagedZIO
  }
}
