package org.digitalrats.flux

import org.digitalrats.flux.configuration.ConfigurationModule.Configuration
import org.digitalrats.flux.dao.DAGRepository
import org.digitalrats.flux.system.DAGScheduler
import org.digitalrats.flux.system.DAGScheduler.SchedulerData
import zio.blocking.Blocking
import zio.clock.Clock
import zio.{ExitCode, URIO, ZIO, ZRef}

object Flux extends zio.App {

  def run(args: List[String]): zio.URIO[zio.ZEnv, ExitCode] = {
    for {
      schedulerData <- ZRef.make(SchedulerData(Map.empty))
      defaultLayer = DAGScheduler.live(schedulerData) ++ Clock.live
      applicationLayer = Blocking.live >+> Configuration.live >+> DAGRepository.live >+> defaultLayer >>> FluxServer.live ++ FluxClient.live
      exitCode <- myAppLogic
        .provideLayer(applicationLayer)
        .exitCode
    } yield exitCode
  }

  // we want a service, but return never
  val myAppLogic: URIO[FluxServer.Service, Unit] = ZIO.never
}
