package org.digitalrats.flux

import io.circe.generic.auto._
import org.digitalrats.flux.dao.DAGRepository
import org.digitalrats.flux.dao.DAGRepository.DAGRepository
import org.digitalrats.flux.model.{DAG, DAGRun}
import org.digitalrats.flux.system.DAGScheduler
import org.digitalrats.flux.system.DAGScheduler.DAGScheduler
import org.http4s.HttpRoutes
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.server.http4s.ztapir.ZHttp4sServerInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import sttp.tapir.ztapir._
import zio.blocking.Blocking
import zio.clock.Clock
import zio.ZIO


object FluxRoutes {

  val getAllDags: ZServerEndpoint[DAGRepository, Any] = {
    endpoint
      .get.in("api" / "v1.0"/ "dags")
      .out(jsonBody[Seq[DAG]])
      .errorOut(jsonBody[DAGRepository.ErrorInfo])
      .serverLogic {_ =>
        ZIO.accessM[DAGRepository](_.get.findAll())
      }
  }

  val getDag: ZServerEndpoint[DAGRepository, Any] = {
    endpoint
      .get.in("api" / "v1.0"/ "dags" /path[Int])
      .out(jsonBody[DAG])
      .errorOut(jsonBody[DAGRepository.ErrorInfo])
      .serverLogic {id =>
        ZIO.accessM[DAGRepository](_.get.find(id.toString))
      }
  }

  val scheduleDag: ZServerEndpoint[DAGRepository with DAGScheduler, Any] = {
    endpoint
      .post.in("api" / "v1.0"/ "dags" / path[Int] / "dagRun")
      .out(jsonBody[DAGRun])
      .errorOut(jsonBody[DAGScheduler.ErrorInfo])
      .serverLogic {id =>
        DAGScheduler.schedule(id.toString)
      }
  }

  type Env = DAGRepository with DAGScheduler with Clock with Blocking

  private val endpoints = List(
    getAllDags.widen[Env],
    getDag.widen[Env],
    scheduleDag.widen[Env]
  )

  private val swaggerEndpoints = SwaggerInterpreter().fromServerEndpoints(endpoints, "Flux", "1.0")

  val routes: HttpRoutes[zio.ZIO[Env, Throwable, *]] =
    ZHttp4sServerInterpreter().from(endpoints).toRoutes // this is where zio-cats interop is needed

  val swaggerRoutes: HttpRoutes[zio.ZIO[Env, Throwable, *]] = ZHttp4sServerInterpreter().from(swaggerEndpoints).toRoutes

}
