package org.digitalrats.flux

import cats.effect.{Async, Resource}
import cats.implicits.catsSyntaxFlatMapOps
import com.comcast.ip4s.{Host, Port}
import org.digitalrats.flux.configuration.ConfigurationModule.Configuration
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import org.http4s.server.middleware.Logger
import zio.{Has, Runtime, Task, ZLayer, ZManaged}
import zio.interop.catz._
import cats.syntax.all._

import zio.blocking.Blocking
import zio.clock.Clock

object FluxServer {
  type Service = Has[Server]
  val live: ZLayer[FluxRoutes.Env with Clock with Blocking with Configuration, Throwable, Service] = ZLayer.fromManaged {
    for {
      // we should probably cache this, and make this a function, where we
      // pass in the relevant configuration
      config <- Configuration.load.toManaged_

      httpApp = (
          FluxRoutes.routes <+>
          FluxRoutes.swaggerRoutes
        ).orNotFound

      // With Middlewares in place
      finalHttpApp = Logger.httpApp(logHeaders = true, logBody = true)(httpApp)

      // The implicit runtime provided the implicits needed to create the ConcurrentEffect.
      // This is done by the zio / cats interop imports.
      server <- ZManaged.runtime[FluxRoutes.Env].flatMap {
        implicit runtime: Runtime[FluxRoutes.Env] => (EmberServerBuilder.default[zio.ZIO[FluxRoutes.Env,Throwable,*]]
          .withHost(Host.fromString(config.api.endpoint).get)
          .withPort(Port.fromInt(config.api.port).get)
          .withHttpApp(finalHttpApp)
          .build >>
          Resource.eval(Async[Task].never)).toManagedZIO
      }
    } yield (server)
  }
}
