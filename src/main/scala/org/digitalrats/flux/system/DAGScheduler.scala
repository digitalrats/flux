package org.digitalrats.flux.system

import org.digitalrats.flux.configuration.ConfigurationModule.Configuration
import org.digitalrats.flux.dao.DAGRepository
import org.digitalrats.flux.dao.DAGRepository.DAGRepository
import org.digitalrats.flux.model.{DAG, DAGRun}
import zio.blocking.Blocking
import zio.{Has, Ref, URLayer, ZIO, ZManaged}

object DAGScheduler {

  case class SchedulerData(executors: Map[DAG,ZManaged[Any, Nothing, Executor]])

  type DAGScheduler = Has[Service]

  case class ErrorInfo(message: String)

  trait Service {
    def schedule(dagId: String): ZIO[DAGRepository,Throwable,Either[ErrorInfo, DAGRun]]
  }

  final case class DAGSchedulerImpl(data: Ref[SchedulerData], blocking: Blocking.Service, dagRepository: DAGRepository.Service) extends Service {
    override def schedule(dagId: String): ZIO[DAGRepository,Throwable,Either[ErrorInfo, DAGRun]] = {
      for {
        eitherDag <- DAGRepository.find(dagId)
        dag <- eitherDag match {
          case Right(dag) => ZIO.succeed(dag)
          case Left(error) => ZIO.fail(new Throwable(error.message))
        }
        SchedulerData(executors) <- data.get
        executor = executors.getOrElse(dag,LocalExecutor.make)
        _ = if (!executors.contains(dag)) {
          data.set(SchedulerData(executors ++ Map(dag->executor)))
        } else ZIO(())
        dagRun <- executor.use(executor => executor.execute(ZIO(Right(DAGRun()))))
      } yield (dagRun)
    }
  }

  def live(data: Ref[SchedulerData]): URLayer[Blocking with Configuration with DAGRepository, DAGScheduler] =
    (DAGSchedulerImpl(data, _,_)).toLayer[Service]

  def schedule(dagId: String): ZIO[DAGRepository with DAGScheduler,Throwable,Either[ErrorInfo, DAGRun]] = {
    ZIO.accessM(_.get[Service].schedule(dagId))
  }



}
