package org.digitalrats.flux.system

import zio.stream.ZStream
import zio.{ Promise, Queue, UIO, ZIO, ZManaged }

object LocalExecutor {
  def make: ZManaged[Any, Nothing, Executor] =
    for {
      queue <- Queue.unbounded[UIO[Unit]].toManaged_
      _     <- ZStream
        .fromQueue(queue)
        .mapMPar(2)(effect => effect)
        .runDrain
        .forkManaged
    } yield new Executor {
      override def execute[R, E, A](effect: ZIO[R, E, A]): ZIO[R, E, A] =
        for {
          promise         <- Promise.make[E, A]
          env             <- ZIO.environment[R]
          effectToPromise  = effect.to(promise).unit
          effectWithoutEnv = effectToPromise.provide(env)
          interrupted     <- Promise.make[Nothing, Unit]
          result          <- (queue.offer(interrupted.await race effectWithoutEnv) *> promise.await)
            .onInterrupt(interrupted.succeed(()))
        } yield result
    }
}
