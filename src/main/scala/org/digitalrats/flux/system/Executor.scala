package org.digitalrats.flux.system

import zio.ZIO

trait Executor {
  def execute[R, E, A](effect: ZIO[R, E, A]): ZIO[R, E, A]
}
