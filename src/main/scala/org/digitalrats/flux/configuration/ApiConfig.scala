package org.digitalrats.flux.configuration

case class ApiConfig(endpoint: String, port: Int)
