package org.digitalrats.flux.configuration

case class Config(api: ApiConfig, dbConfig: DbConfig)
