package org.digitalrats.flux.configuration

case class DbConfig(url: String, user: String, password: String)
