package org.digitalrats.flux.configuration

import pureconfig._
import pureconfig.generic.auto._
import zio.{Layer, Task, ZIO, ZLayer}

object ConfigurationModule {
  type Configuration = zio.Has[Configuration.Service]

  object Configuration {

    trait Service {
      val load: Task[Config]
    }

    val live: Layer[Nothing, Configuration] = ZLayer.succeed {
      new Service {
        override val load: Task[Config] = Task.fromEither(
          ConfigSource.default
            .load[Config]
            .left
            .map(pureconfig.error.ConfigReaderException.apply)
        )
      }
    }

    val load: ZIO[Configuration, Throwable, Config] = ZIO.accessM(_.get.load)
  }

}
