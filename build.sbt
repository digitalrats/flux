ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.7"

lazy val ZIOVersion = "1.0.12"
lazy val ZIOLoggingVersion = "0.5.14"
lazy val Http4sVersion = "0.23.7"
val TapirVersion = "0.20.0-M4"
val CirceVersion = "0.14.1"
val DoobieVersion = "1.0.0-RC1"
val LogbackVersion = "1.2.10"

lazy val zio = Seq(
  "dev.zio"    %% "zio"                 % ZIOVersion,
  "dev.zio"    %% "zio-test"            % ZIOVersion % "test",
  "dev.zio"    %% "zio-test-sbt"        % ZIOVersion % "test",
  "dev.zio"    %% "zio-interop-cats"    % "3.2.9.0",
  "dev.zio"    %% "zio-logging"         % ZIOLoggingVersion,
  "dev.zio"    %% "zio-logging-slf4j"   % ZIOLoggingVersion,
  "dev.zio"    %% "zio-streams"         % ZIOVersion,
  "dev.zio"    %% "zio-kafka"           % "0.17.3"
)

lazy val http4s = Seq(
  "http4s-ember-server",
  "http4s-ember-client",
  "http4s-dsl",
  "http4s-circe"
).map("org.http4s" %% _ % Http4sVersion)

lazy val tapir = Seq(
  "tapir-zio-http4s-server",
  "tapir-json-circe",
  "tapir-swagger-ui-bundle"
).map("com.softwaremill.sttp.tapir" %% _ % TapirVersion)

lazy val doobie = Seq(
  "doobie-core",
  "doobie-h2",
  "doobie-hikari",          // HikariCP transactor.
  "doobie-postgres"
).map("org.tpolecat" %% _ % DoobieVersion)

lazy val circe = Seq(
  "circe-generic"
).map("io.circe" %% _ % CirceVersion)

lazy val ignite = Seq(
  "ignite-core",
  "ignite-slf4j"
).map("org.apache.ignite" % _ % "2.11.1")

lazy val sttp = Seq(
  "core",
  "circe",
  "http4s-backend"
).map("com.softwaremill.sttp.client3" %% _ % "3.3.17")

lazy val root = (project in file("."))
  .settings(
    name := "flux",
    libraryDependencies ++= zio ++ circe ++ http4s ++ sttp ++ tapir ++ doobie ++ ignite ++ Seq(
      "com.lihaoyi" % "ammonite" % "2.5.0" cross CrossVersion.full,
      "com.github.pureconfig" %% "pureconfig"    % "0.17.1",
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.13.2" cross CrossVersion.full),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
  )
